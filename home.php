<?php
print '
        <section id="blogging">
            <div class="container">
                <h1>Blogging Site</h1>
                <p>Pružite svom blogu savršen dom. Nabavite besplatnu domenu blogspot.com ili kupite prilagođenu domenu u samo nekoliko klikova..</p>
                <p>Zarađujte od svog predanog rada. Google AdSense može automatski prikazivati relevantne ciljane oglase na vašem blogu da biste objavljivanjem sadržaja o onome što volite mogli ostvariti dobit.</p>
                <p>Sačuvajte trenutke koji su važni. Blogger vam omogućuje da besplatno pohranite tisuće postova, fotografija i drugih sadržaja putem Googlea.</p>
            </div>
        </section>

        <section id="boxes">
            <div class="container">
                <div class="box">
                    <figure>
                        <img src="./img/blog_icon.png" alt="Blog icon" title="Blog Icon">
                        <figcaption>
                            <h3>Blog Icon</h3>
                            <p>Sačuvajte trenutke koji su važni. Blogger vam omogućuje da besplatno pohranite tisuće postova, fotografija i drugih sadržaja putem Googlea.</p>
                        </figcaption>
                    </figure>
                </div>
                <div class="box">
                    <figure>
                        <img src="./img/blog_news.png" alt="Blog news" title="Blog News">
                        <figcaption>
                            <h3>Blog News</h3>
                            <p>Pružite svom blogu savršen dom. Nabavite besplatnu domenu blogspot.com ili kupite prilagođenu domenu u samo nekoliko klikova.</p>
                        </figcaption>
                    </figure>
                </div>
                <div class="box">
                    <figure>
                        <img src="./img/blog_notes.png" alt="Design" title="Design">
                        <figcaption>
                            <h3>Graphic Design</h3>
                            <p>Stvorite lijep blog koji odgovara vašem stilu. Odaberite jedan od mnogih predložaka iz naše ponude koji su jednostavni za upotrebu te sadrže fleksibilne izglede i stotinu pozadinskih slika ili izradite nešto novo.</p>
                        </figcaption>
                    </figure>
                </div>
            </div>
        </section>';

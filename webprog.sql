CREATE TABLE `countries` (
  `id` int(4) NOT NULL PRIMARY KEY AUTO_INCEREMENT,
  `code` char(2) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `countries` (`code`, `name`) VALUES
('HR', 'Croatia'),
('DE', 'Germany'),
('US', 'United States'),
('GB', 'United Kingdom'),
('AU', 'Australia'),
('RU', 'Russian Federation');


CREATE TABLE `users` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCEREMENT,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `country_id` int(4) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `users` (`firstname`, `lastname`, `username`, `password`, `email`, `country_id`) VALUES
('Mateo', 'Milicevic', 'mmilicevic', 'admin123', 'mmilicevi@tvz.hr', 1);

CREATE TABLE `blog` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCEREMENT,
  `username` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  'subject' TEXT NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

<?php
function pickerDateToMysql($pickerDate)
{
	$date = DateTime::createFromFormat('Y-m-d H:i:s', $pickerDate);
	return $date->format('d.m.Y H:i:s');
}

function pickerDateToJson($pickerDate)
{
	$date = new DateTime($pickerDate);
	return $date->format('d-m-Y H:i:s');
}

<?php
print '
    <div class="container">
        <div id="register_form">
            <div class="grey">
                <h1>Registration Form</h1>';

if ($_POST['_action_'] == FALSE) {
    print '
                    <form action="" class="submit_form" name="registration_form" method="POST">
                        <input type="hidden" name="_action_" value="TRUE">
                        <div class="col">
                            <label for="fname">First Name *</label><br>
                            <input type="text" name="fname" placeholder="Your first name..." required>
                        </div>
                        <div class="col">
                            <label for="lname">Last Name *</label><br>
                            <input type="text" name="lname" placeholder="Your last name..." required>
                        </div>
                        <div class="col">
                            <label for="email">Email *</label><br>
                            <input type="email" name="email" placeholder="Your e-mail address" required>
                        </div>
                        <div class="col">
                            <label for="username">Username:* <small>(Username must have min 5 and max 10 char)</small></label><br>
                            <input type="text" name="username" pattern=".{5,10}" placeholder="Your Username" required>
                        </div>

                        <div class="col">
                            <label for="password">Password:* <small>(Password must have min 4 char)</small></label><br>
                            <input type="password" name="password" placeholder="Password.." pattern=".{4,}" required>
                        </div>

                        <div class="col">
                            <label style=width for="country">Country</label><br>
                            <select name="country">
                                <option value="">Please select</option>';
    #Select all countries from database webprog, table countries
    $query  = "SELECT id, name FROM countries";
    $result = @mysqli_query($MySQL, $query);
    while ($row = @mysqli_fetch_array($result)) {
        print '<option value="' . $row['id'] . '">' . $row['name'] . '</option>';
    }
    print '
                            </select>
                        </div>
                        <button class="submit_btn" type="submit">Submit</button>
                    </form>';
} else if ($_POST['_action_'] == TRUE) {
    $query  = "SELECT * FROM users";
    $query .= " WHERE email='" .  $_POST['email'] . "'";
    $query .= " OR username='" .  $_POST['username'] . "'";
    $result = @mysqli_query($MySQL, $query);
    $row = @mysqli_fetch_array($result, MYSQLI_ASSOC);

    if ($row['email'] == '' || $row['username'] == '') {
        # password_hash() creates a new password hash using a strong one-way hashing algorithm
        $pass_hash = password_hash($_POST['password'], PASSWORD_DEFAULT, $options);

        //Provjera ako country nije poslan, onda se sprema u bazu kao null vrijednost
        if ($_POST['country'] === '') {
            $_POST['country'] = null; // or 'NULL' for SQL
        }

        $query  = "INSERT INTO users (firstname, lastname, email, username, password, country_id)";
        $query .= " VALUES ('" . $_POST['fname'] . "', '" . $_POST['lname'] . "', '" . $_POST['email'] . "', '" . $_POST['username'] . "', '" . $pass_hash . "', '" . $_POST['country'] . "')";
        $result = @mysqli_query($MySQL, $query);

        # ucfirst() — Make a string's first character uppercase
        # strtolower() - Make a string lowercase
        echo '<p>' . ucfirst(strtolower($_POST['fname'])) . ' ' .  ucfirst(strtolower($_POST['lname'])) . ', thank you for registration </p>
                        <hr>';
    } else {
        echo '<p>User with this email or username already exist!</p>';
    }
}
print '
            </div>
        </div>
    </div>';

# Close MySQL connection
@mysqli_close($MySQL);

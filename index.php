<?php

# Stop Hacking attemp
define('__APP__', TRUE);

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

#Start Session
session_start();

# Database connection
include("dbconn.php");

if (isset($_GET['menu'])) {
  $menu   = (int)$_GET['menu'];
}

if (isset($_GET['action'])) {
  $action   = (int)$_GET['action'];
}

if (!isset($_POST['_action_'])) {
  $_POST['_action_'] = FALSE;
}

if (!isset($menu)) {
  $menu = 1;
}

$options = [
  'cost' => 12,
];

include_once("functions.php");

print '
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Professional blog site">
  <meta name="keywords" content="blog design, professional blog site">
  <meta name="author" content="Mateo Milicevic">
  <title>Professional Blog Site | Welcome</title>
  <link rel="stylesheet" href="./css/style.css">
</head>

<body>
  <header>
    <div class="container">
      <div id="branding">
        <h1><span class="highlight">Professional</span> Blog Site</h1>
      </div>
      <nav>';
include("menu.php");
print '
      </nav>
    </div>
  </header>

  <main>';

switch ($menu) {
    # Homepage
  case 1:
    include("home.php");
    break;

    # About Us
  case 2:
    include("about-us.php");
    break;

    # Blog
  case 3:
    include("blog.php");
    break;

    # Contact
  case 4:
    include("contact.php");
    break;

    # Register
  case 5:
    include("register.php");
    break;

    # Sign In
  case 6:
    include("signin.php");
    break;

    # Admin
  case 7:
    include("admin.php");
    break;

    # Blogger API
  case 8:
    include("blogApi.php");
    break;
}

print '
  </main>

  <footer>
    <p>
      <a href="https://www.linkedin.com" target="_blank"><img src="img/linkedin.svg" alt="Linkedin" title="Linkedin" style="width:24px; margin-top:0.4em"></a>
      <a href="https://twitter.com" target="_blank"><img src="img/twitter.svg" alt="Twitter" title="Twitter" style="width:24px; margin-top:0.4em"></a>
      <a href="https://plus.google.com" target="_blank"><img src="img/google+.svg" alt="Google+" title="Google+" style="width:24px; margin-top:0.4em"></a>
    </p>
    <p>Acme Web Deisgn, Copyright &copy; ' . date("Y") . '</p>
  </footer>
</body>

</html>';

<?php
$key = 'AIzaSyAE4uBccIex9rzg63nDsRepET_dYMnKIAo';
if (isset($action) && $action != '') {
    $url = 'https://www.googleapis.com/blogger/v3/blogs/2399953/posts/' . $action . '?key=' . $key;
    $json = file_get_contents($url);
    $_data = json_decode($json, true);

    if (isset($_data['title']) && $_data['title'] != '') {
        print '
        <section>
            <div class="container">
                <article id="main-col">
                    <h1>' . $_data['title'] . '</h1>
                    <div class="grey">
                        <p>' . $_data['content'] . '</p>
                        <p>Napisao: ' . $_data['author']['displayName'] . '</p>
                        <p><time datetime="' . $_data['published'] . '">' . pickerDateToJson($_data['published']) . '</time></p>
                    </div>
                    <button type="button" class="back_btn"><a href="index.php?menu=' . $menu . '" class="AddLink">Back</a></button>
                </article>
            </div>
        </section>';
    } else {
        echo '<strong>ERROR 404</strong>';
    }
} else {
    $url = 'https://www.googleapis.com/blogger/v3/blogs/2399953/posts?key=' . $key;

    $json = file_get_contents($url);
    $_data = json_decode($json, true);
    $_posts = $_data['items'];

    if (isset($_posts) && !empty($_posts)) {
        print '
        <section>
            <div class="container">
                <article id="main-col">
                    <h1>Google Blogger API</h1>
                    <ul id="blogs">';
        foreach ($_posts as $blog) {
            print '
                        <li>
                            <h2>' . $blog['title'] . '</h2>';
            if (strlen($blog['content']) > 300) {
                echo substr(strip_tags($blog['content']), 0, 300) . '... <a href="index.php?menu=' . $menu . '&amp;action=' . $blog['id'] . '">More</a>';
            } else {
                echo strip_tags($blog['content']);
            }
            print '
                            <p>Napisao: ' . $blog['author']['displayName'] . '</p>
                            <p><time datetime="' . $blog['published'] . '">' . pickerDateToJson($blog['published']) . '</time></p>
                        </li>
            ';
        }
    } else {
        echo '<strong>ERROR 404</strong>';
    }
    print '
                </ul>
            </article>
        </div>
    </section>';
}

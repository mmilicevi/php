<?php
print '
   <section>
      <div class="container">
        <article id="main-col">
          <h1 class="page-title">About Us</h1>
          <p>
            Stvorite lijep blog koji odgovara vašem stilu. 
            Odaberite jedan od mnogih predložaka iz naše ponude koji su jednostavni za upotrebu te sadrže fleksibilne izglede i 
            stotinu pozadinskih slika ili izradite nešto novo.
          </p>
          <p>
            Saznajte koji su postovi popularni pomoću Bloggerove ugrađene analitike. 
            Saznat ćete odakle dolazi vaša publika i koji su njihovi interesi. 
            Za detaljniji uvid svoj blog možete izravno povezati i na Google Analytics.
          </p>
          <div id="video-box">
            <video controls poster="img/codingIsEasy.jpg">
              <source src="video/codingIsEasy.mp4" type="video/mp4">
              Your browser does not support HTML5 video.
            </video>
          </div>
        </article>

        <aside id="sidebar">
          <div class="grey">
            <h3>What We Do</h3>
            <p>Bez obzira na to želite li podijeliti svoje stručno znanje, 
            aktualne novosti ili ono što vam je na umu, na Bloggeru ste u dobrom društvu. 
            Registrirajte se kako biste saznali zašto milijuni ljudi ovdje objavljuju sadržaj o onome što vole.</p>
          </div>
        </aside>
      </div>
    </section>';

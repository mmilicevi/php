<?php

if ($_POST['_action_'] == TRUE) {
    $to      = 'mmilicevic0123@gmail.com';
    $subject = $_POST['title'] . ' - ' . $_POST['fname'] . ' ' . $_POST['lname'] . ' (' . $_POST['country'] . ')';
    $message = $_POST['subject'];
    $headers = 'From: ' . $_POST['email'] . "\r\n" .
        'Reply-To: ' . $_POST['email'] . "\r\n" .
        'X-Mailer: PHP/' . phpversion();

    mail($to, $subject, $message, $headers);

    $_SESSION['message'] = '<p>Poruka je uspješno poslana na mail!</p>';
}

print '
        <section>
            <div class="container">
                <article id="main-col">';
if (isset($_SESSION['message'])) {
    print  $_SESSION['message'];
    unset($_SESSION['message']);
}
print '
                    <h1 class="page-title">Contact</h1>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2781.7890741539636!2d15.966758816056517!3d45.795453279106205!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4765d68b5d094979%3A0xda8bfa8459b67560!2sUl.+Vrbik+VIII%2C+10000%2C+Zagreb!5e0!3m2!1shr!2shr!4v1509296660756" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
                </article>

                <aside id="sidebar">
                    <div class="grey">
                        <h3>Contact Form</h3>
                        <form class="submit_form" name="contact_form" method="POST">
                            <input type="hidden" name="_action_" value="TRUE">
                            <div class="col">
                                <label for="fname">First Name *</label><br>
                                <input type="text" name="fname" placeholder="Your first name..." required>
                            </div>
                            <div class="col">
                                <label for="lname">Last Name *</label><br>
                                <input type="text" name="lname" placeholder="Your last name..." required>
                            </div>
                            <div class="col">
                                <label>Email *</label><br>
                                <input type="email" name="email" placeholder="Your e-mail address" required>
                            </div>
                            <div class="col">
                                <label for="country">Country</label><br>
                                <select name="country">
                                    <option value="">Please select</option>';
#Select all countries from database webprog, table countries
$query  = "SELECT * FROM countries";
$result = @mysqli_query($MySQL, $query);
while ($row = @mysqli_fetch_array($result)) {
    print '<option value="' . $row['code'] . '">' . $row['name'] . '</option>';
}
# Close MySQL connection
@mysqli_close($MySQL);
print '
                                </select>
                            </div>
                            <div class="col">
                                <label for="title">Title *</label><br>
                                <input name="title" placeholder="Title.." required></input>
                            </div>
                            <div class="col">
                                <label for="subject">Message *</label><br>
                                <textarea name="subject" placeholder="Write something.." style="height:150px" required></textarea>
                            </div>
                            <button class="submit_btn" type="submit">Submit</button>
                        </form>
                    </div>
                </aside>
            </div>
        </section>';

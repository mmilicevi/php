<?php
if (isset($action) && $action != '') {
  $query  = "SELECT * FROM blog";
  $query .= " WHERE id=" . $_GET['action'];
  $result = @mysqli_query($MySQL, $query);
  $row = @mysqli_fetch_array($result, MYSQLI_ASSOC);
  print '
  <section>
    <div class="container">
      <article id="main-col">
        <h1>' . $row['title'] . '</h1>
        <div class="grey">
          <p>'  . $row['subject'] . '</p>
          <p>Napisao: ' . $row['username'] . '</p>
          <p><time datetime="' . $row['timestamp'] . '">' . pickerDateToMysql($row['timestamp']) . '</time></p>
        </div>
        <button type="button" class="back_btn"><a href="index.php?menu=' . $menu . '" class="AddLink">Back</a></button>
      </article>
    </div>
  </section>';
} else if ($_POST['_action_'] == TRUE) {
  $query  = "INSERT INTO blog (username, title, subject)";
  $query .= " VALUES ('" . $_POST['username'] . "', '" . $_POST['title'] . "', '" . $_POST['subject'] . "')";
  $result = @mysqli_query($MySQL, $query);
  header("Location: index.php?menu=3");
} else {
  $query  = "SELECT * FROM blog";
  $query .= " ORDER BY timestamp DESC";
  $result = @mysqli_query($MySQL, $query);
  print '
    <section>
      <div class="container">
        <article id="main-col">
          <h1>Blog</h1>
          <ul id="blogs">';
  while ($row = @mysqli_fetch_array($result)) {
    print '
            <li>
              <h2>' . $row['title'] . '</h2>';
    if (strlen($row['subject']) > 300) {
      echo substr(strip_tags($row['subject']), 0, 300) . '... <a href="index.php?menu=' . $menu . '&amp;action=' . $row['id'] . '">More</a>';
    } else {
      echo strip_tags($row['subject']);
    }
    print '
              <p>Napisao: ' . $row['username'] . '</p>
              <p><time datetime="' . $row['timestamp'] . '">' . pickerDateToMysql($row['timestamp']) . '</time></p>
            </li>';
  }
  print '         
          </ul>
        </article>

        <aside id="sidebar">
          <div class="grey">
            <h3>Post a blog</h3>
            <form class="submit_form" name="blog_form" method="POST">
              <input type="hidden" name="_action_" value="TRUE">
              <div class="col">
                <label for="username">Username *</label><br>
                <input type="text" name="username" placeholder="Your username..." required>
              </div>
              <div class="col">
                <label for="title">Title *</label><br>
                <input type="text" name="title" placeholder="Enter a title..." required>
              </div>
              <div class="col">
                <label for="subject">Message *</label><br>
                <textarea name="subject" placeholder="Write something.." style="height:150px" required></textarea>
              </div>
              <button class="submit_btn" type="submit">Submit</button>
            </form>
          </div>
        </aside>
      </div>
    </section>';
}

# Close MySQL connection
@mysqli_close($MySQL);

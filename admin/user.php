<?php
$user_id = $_SESSION['user']['id'];

if ($_POST['_action_'] == 'TRUE') {

    //provjera postoji li user s istim username-om ili email-om, ali se ne gledaju njegovi podatci u bazi
    $query  = "SELECT * FROM users";
    $query .= " WHERE (email='" .  $_POST['email'] . "'";
    $query .= " OR username='" .  $_POST['username'] . "') AND";
    $query .= " id<>'" . $user_id . "'";
    $result = @mysqli_query($MySQL, $query);
    $user = @mysqli_fetch_array($result, MYSQLI_ASSOC);

    if ($user['email'] == '' || $user['username'] == '') {
        //Provjera ako country nije poslan, onda se sprema u bazu kao null vrijednost
        if ($_POST['country'] == '') {
            $_POST['country'] = null; // or 'NULL' for SQL
        }

        $query  = "UPDATE users SET firstname='" . $_POST['fname'] . "', lastname='" . $_POST['lname'] . "', email='" . $_POST['email'] . "', username='" . $_POST['username'] . "', country_id='" . $_POST['country'] . "'";
        $query .= " WHERE id=" . $user_id;
        $result = @mysqli_query($MySQL, $query);

        // Ponovno se dodaju ime, prezime i username u sessiju jer ih user moze ažurirati
        $_SESSION['user']['firstname'] = $_POST['fname'];
        $_SESSION['user']['lastname'] = $_POST['lname'];
        $_SESSION['user']['username'] = $_POST['username'];
        $_SESSION['message'] = '<p>' . $_SESSION['user']['username'] . ' have successfully changed profile!</p>';
    } else {
        $_SESSION['message'] = '<p>User with this email or username already exist!</p>';
    }
}

$query  = "SELECT * FROM users";
$query .= " WHERE id=" . (int)$user_id;
$result = @mysqli_query($MySQL, $query);
$row = @mysqli_fetch_array($result, MYSQLI_ASSOC);

print '
    <div class="container">
        <div id="admin_form">';
if (isset($_SESSION['message'])) {
    print '<h3><strong>' . $_SESSION['message'] . '</strong></h3>';
    unset($_SESSION['message']);
}
print '
            <div class="grey">
                <h1>' . $row['username']  . '</h1>
                <form action="" class="submit_form" name="user_form_edit" method="POST">
                    <input type="hidden" name="_action_" value="TRUE">
                
                    <div class="col">
                        <label for="fname">First Name *</label>
                        <input type="text" name="fname" value="' . $row['firstname'] . '" placeholder="Your name.." required>
                    </div>
                    <div class="col">
                        <label for="lname">Last Name *</label>
                        <input type="text" name="lname" value="' . $row['lastname'] . '" placeholder="Your last natme.." required>
                    </div>
                    <div class="col">
                        <label for="email">Your E-mail *</label>
                        <input type="email" name="email"  value="' . $row['email'] . '" placeholder="Your e-mail.." required>
                    </div>
                    <div class="col">
                        <label for="username">Username *<small>(Username must have min 5 and max 10 char)</small></label>
                        <input type="text" name="username" value="' . $row['username'] . '" pattern=".{5,10}" placeholder="Username.." required><br>
                    </div>
                    <div class="col">
                        <label for="country">Country</label>
                        <select name="country" id="country">
                            <option value="">molimo odaberite</option>';
#Select all countries from database webprog, table countries
$_query  = "SELECT * FROM countries";
$_result = @mysqli_query($MySQL, $_query);
while ($_row = @mysqli_fetch_array($_result)) {
    print '<option value="' . $_row['id'] . '"';
    if ($row['country_id'] == $_row['id']) {
        print ' selected';
    }
    print '>' . $_row['name'] . '</option>';
}
print '
                        </select>
                    </div>
        
                    <button class="submit_btn" type="submit">Submit</button>
                </form>
            </div>
        </div>
    </div>';
# Close MySQL connection
@mysqli_close($MySQL);

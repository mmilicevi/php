<?php

#Add blog
if (isset($_POST['_action_']) && $_POST['_action_'] == 'add_blog') {
    $_SESSION['message'] = '';
    $query  = "INSERT INTO blog (username, title, subject)";
    $query .= " VALUES ('" . $_POST['username'] . "', '" . $_POST['title'] . "', '" . $_POST['subject'] . "')";
    $result = @mysqli_query($MySQL, $query);

    $_SESSION['message'] .= '<p>You have successfully added <strong>' . strtoupper($_POST['title'])  . '</strong> blog!</p>';

    # Redirect
    header("Location: index.php?menu=7&action=2");
}

# Update blog
else if (isset($_POST['_action_']) && $_POST['_action_'] == 'edit_blog') {
    $query  = "UPDATE blog SET title='" . $_POST['title'] . "', subject='" . $_POST['subject'] . "'";
    $query .= " WHERE id=" . (int)$_POST['edit'];
    $result = @mysqli_query($MySQL, $query);

    $_SESSION['message'] = '<p>You have successfully changed  <strong>' . strtoupper($_POST['title'])  . '</strong> blog!</p>';

    # Redirect
    header("Location: index.php?menu=7&action=2");
}


# Delete blog
else if (isset($_GET['delete']) && $_GET['delete'] != '') {

    # Select deleted blog
    $query  = "SELECT * FROM blog";
    $query .= " WHERE id=" . $_GET['delete'];
    $result = @mysqli_query($MySQL, $query);
    $row = @mysqli_fetch_array($result, MYSQLI_ASSOC);

    # Delete blog
    $query  = "DELETE FROM blog";
    $query .= " WHERE id=" . (int)$_GET['delete'];
    $result = @mysqli_query($MySQL, $query);

    $_SESSION['message'] = '<p>You have successfully deleted <strong>' . strtoupper($row['title'])  . '</strong> blog!</p>';
}

#Show blog info
if (isset($_GET['id']) && $_GET['id'] != '') {
    $query  = "SELECT * FROM blog";
    $query .= " WHERE id=" . $_GET['id'];
    $query .= " ORDER BY timestamp DESC";
    $result = @mysqli_query($MySQL, $query);
    $row = @mysqli_fetch_array($result);
    print '
        <section>
            <div class="container">
                <article id="main-col">
                    <h3>' . $row['title'] . '</h3>
                    <div class="grey">
                        <p>' . $row['subject'] . '</p>
                        <p>Napisao: ' . $row['username'] . '</p>
                        <p><time datetime="' . $row['timestamp'] . '">' . pickerDateToMysql($row['timestamp']) . '</time></p>
                    </div>
                    <button type="button" class="back_btn"><a href="index.php?menu=' . $menu . '&amp;action=' . $action . '" class="AddLink">Back</a></button>
                </article>
            </div>
        </section>';
}

#Add blog 
else if (isset($_GET['add']) && $_GET['add'] != '') {
    $username = $_SESSION['user']['username'];
    print '
		<div class="container">
            <div id="admin_form">
                <div class="grey">
                    <h1>Add Blog</h1>
		            <form action="" class="submit_form" name="blog_form_add" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="_action_" value="add_blog">
                        <input type="hidden" name="username" value="' . $username . '">
			
                        <div class="col">
                            <label for="title">Title *</label><br>
                            <input type="text" name="title" placeholder="Enter a title..." required>
                        </div>
                        <div class="col">
                            <label for="subject">Message *</label><br>
                            <textarea name="subject" placeholder="Write something.." style="height:150px" required></textarea>
                        </div>
			
                        <button class="submit_btn" type="submit">Submit</button>
		            </form>
                </div>
                <button type="button" class="back_btn"><a href="index.php?menu=' . $menu . '&amp;action=' . $action . '" class="AddLink">Back</a></button>
            </div>
        </div>';
}
#Edit blog
else if (isset($_GET['edit']) && $_GET['edit'] != '') {
    $query  = "SELECT * FROM blog";
    $query .= " WHERE id=" . $_GET['edit'];
    $result = @mysqli_query($MySQL, $query);
    $row = @mysqli_fetch_array($result, MYSQLI_ASSOC);

    print '
		<div class="container">
            <div id="admin_form">
                <div class="grey">
                    <h1>Edit Blog</h1>
		            <form action="" class="submit_form" name="blog_form_edit" method="POST" enctype="multipart/form-data">
			            <input type="hidden" name="_action_" value="edit_blog">
			            <input type="hidden" name="edit" value="' . $row['id'] . '">
			
                        <div class="col">
                            <label for="title">Title *</label><br>
                            <input type="text" name="title" value="' . $row['title'] . '" required>
                        </div>
                        <div class="col">
                            <label for="subject">Message *</label><br>
                            <textarea name="subject" style="height:150px" required>' . $row['subject'] . '</textarea>
                        </div>
			
                        <button class="submit_btn" type="submit">Submit</button>
		            </form>
                </div>
                <button type="button" class="back_btn"><a href="index.php?menu=' . $menu . '&amp;action=' . $action . '" class="AddLink">Back</a></button>
            </div>
        </div>';
} else {
    $query  = "SELECT * FROM blog";
    $query .= " ORDER BY timestamp DESC";
    $result = @mysqli_query($MySQL, $query);
    print '
        <section>
            <div class="container">
                <article id="main-col">';
    if (isset($_SESSION['message'])) {
        print '<h3><strong>' . $_SESSION['message'] . '</strong></h3>';
        unset($_SESSION['message']);
    }
    print '
                    <h1>Blogs</h1>
                    <button type="button" class="submit_btn"><a href="index.php?menu=' . $menu . '&amp;action=' . $action . '&amp;add=true" class="AddLink">Add Blog</a></button>
                    <ul id="blogs">';
    while ($row = @mysqli_fetch_array($result)) {
        print '
                        <li>
                            <h2>' . $row['title'] . '</h2>';
        if (strlen($row['subject']) > 300) {
            echo substr(strip_tags($row['subject']), 0, 300) . '... <a href="index.php?menu=' . $menu . '&amp;action=' . $action . '&amp;id=' . $row['id'] . '">More</a>';
        } else {
            echo strip_tags($row['subject']);
        }
        print '
                            <p>Napisao: ' . $row['username'] . '</p>
                            <p><time datetime="' . $row['timestamp'] . '">' . pickerDateToMysql($row['timestamp']) . '</time></p>
                            <p><a href="index.php?menu=' . $menu . '&amp;action=' . $action . '&amp;edit=' . $row['id'] . '"><img src="img/edit.png" alt="uredi"></a></p>
                            <p><a href="index.php?menu=' . $menu . '&amp;action=' . $action . '&amp;delete=' . $row['id'] . '"><img src="img/delete.png" alt="obriši"></a></p>
                        </li>';
    }
    print '
                    </ul>
                </article>
            </div>
        </section>';
}

# Close MySQL connection
@mysqli_close($MySQL);

<?php
if ($_SESSION['user']['valid'] == 'true') {
    if (!isset($action)) {
        $action = 1;
    }
    print '
    <section id="admin">
        <div class="container">
            <div id="nav">
                <ul>
                    <li';
    ($action == 1) ? print ' class="current"' : print '';
    print '><a href="index.php?menu=7&amp;action=1">User</a></li>
                    <li';
    ($action == 2) ? print ' class="current"' : print '';
    print '><a href="index.php?menu=7&amp;action=2">Blog</a></li>
                </ul>
            </div>
        </div>
    </section>';

    if ($action == 1) {
        include("admin/user.php");
    } else if ($action == 2) {
        include("admin/blog.php");
    }
} else {
    $_SESSION['message'] = '<p>Please register or login using your credentials!</p>';
    header("Location: index.php?menu=6");
}

<?php

print '
    <div class="container">
        <div id="signin_form">
            <div class="grey">
                <h1>Sign In</h1>';
if ($_POST['_action_'] == FALSE) {
    if (isset($_SESSION['message'])) {
        print '<h3><strong>' . $_SESSION['message'] . '</strong></h3>';
        unset($_SESSION['message']);
    }
    print '
                    <form action="" class="submit_form" name="login_form" method="POST">
                        <input type="hidden" name="_action_" value="TRUE">
                        <div class="col">
                            <label for="username">Username</small></label><br>
                            <input type="text" name="username" pattern=".{5,10}" required>
                        </div>

                        <div class="col">
                            <label for="password">Password</label><br>
                            <input type="password" name="password" pattern=".{4,}" required>
                        </div>
                        <button class="submit_btn" type="submit">Submit</button>
                    </form>';
} else if ($_POST['_action_'] == TRUE) {
    $query  = "SELECT * FROM users";
    $query .= " WHERE username='" .  $_POST['username'] . "'";
    $result = @mysqli_query($MySQL, $query);
    $row = @mysqli_fetch_array($result, MYSQLI_ASSOC);

    if (password_verify($_POST['password'], $row['password'])) {
        #password_verify https://secure.php.net/manual/en/function.password-verify.php
        $_SESSION['user']['valid'] = 'true';
        $_SESSION['user']['id'] = $row['id'];
        $_SESSION['user']['firstname'] = $row['firstname'];
        $_SESSION['user']['lastname'] = $row['lastname'];
        $_SESSION['user']['username'] = $row['username'];
        $_SESSION['message'] = '<p>Dobrodošli, ' . $_SESSION['user']['firstname'] . ' ' . $_SESSION['user']['lastname'] . '!</p>';
        # Redirect to admin website
        header("Location: index.php?menu=7");
    }

    # Bad username or password
    else {
        unset($_SESSION['user']);
        $_SESSION['message'] = '<p>You entered wrong username or password!</p>';
        header("Location: index.php?menu=6");
    }
}
print '
            </div>
        </div>
    </div>';

# Close MySQL connection
@mysqli_close($MySQL);
